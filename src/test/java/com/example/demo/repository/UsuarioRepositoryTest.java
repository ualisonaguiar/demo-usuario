package com.example.demo.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.Usuario;

@SpringBootTest
public class UsuarioRepositoryTest {

    @Autowired
    UsuarioRepository repository;

    @Test
    public void cadastrar() {
        Usuario usuario = getEntitidade("2º Ten Ualison", "dtiadj2@dct.eb.mil.br");

        repository.save(usuario);
        assertNotNull(repository.findByEmail(usuario.getEmail()));
    }

    @Test
    public void editar() {
        Usuario usuario = getEntitidade("2º Ten Leandro", "dtiadj5@dct.eb.mil.br");
        repository.save(usuario);
        usuario = repository.findByEmail(usuario.getEmail());

        usuario.setEmail("dtiajd3@dct.eb.mil.br");
        usuario.setNome("2º Ten Riocemar");
        repository.save(usuario);

        assertNotNull(repository.findByEmail(usuario.getEmail()));
        assertNull(repository.findByEmail("dtiadj5@dct.eb.mil.br"));
    }

    @Test
    public void listagem() {
        repository.deleteAll();

        repository.save(getEntitidade("2º Ten Riocemar", "dtiadj3@dct.eb.mil.br"));
        repository.save(getEntitidade("2º Ten Ualison", "dtiadj2@dct.eb.mil.br"));
        repository.save(getEntitidade("2º Ten Leandro", "dtiadj5@dct.eb.mil.br"));

        assertFalse(repository.findAll().isEmpty());

        assertNotNull(repository.findByEmail("dtiadj2@dct.eb.mil.br"));
        assertNotNull(repository.findByEmail("dtiadj3@dct.eb.mil.br"));
        assertNotNull(repository.findByEmail("dtiadj5@dct.eb.mil.br"));

        assertEquals(3, repository.findAll().size());
    }

    @Test
    public void excluir() {
        repository.save(getEntitidade("Cap Pinheiro", "dtiadj6@dct.eb.mil.br"));
        Usuario usuario = repository.findByEmail("dtiadj6@dct.eb.mil.br");
        repository.delete(usuario);
        usuario = repository.findByEmail("dtiadj6@dct.eb.mil.br");
        assertNull(usuario);
    }

    protected Usuario getEntitidade(String nome, String email) {
        Usuario usuario = new Usuario();
        usuario.setNome(nome);
        usuario.setEmail(email);

        return usuario;
    }
}
