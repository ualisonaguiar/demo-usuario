package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Usuario;
import com.example.demo.repository.UsuarioRepository;

@RestController
@RequestMapping(value = "/usuario")
public class UsuarioController {

    @Autowired
    UsuarioRepository repository;

    @GetMapping
    public List<Usuario> listagem() {
        return repository.findAll();
    }

    @GetMapping(path = { "/{id}" })
    public ResponseEntity detalhe(@PathVariable("id") long id) {
        return repository.findById(id).map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Usuario inserir(@RequestBody Usuario usuario) {
        return repository.save(usuario);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity atualizar(@PathVariable("id") long id, @RequestBody Usuario usuario) {
        return repository.findById(id).map(record -> {
            record.setEmail(usuario.getEmail());
            record.setNome(usuario.getNome());

            return ResponseEntity.ok().body(repository.save(record));
        }).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping(path = { "/{id}" })
    public ResponseEntity excluir(@PathVariable("id") long id) {
        return repository.findById(id).map(record -> {
            repository.deleteById(record.getId());

            return ResponseEntity.ok().build();
        }).orElse(ResponseEntity.notFound().build());
    }
}
